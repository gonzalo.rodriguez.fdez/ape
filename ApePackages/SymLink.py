"""
Builder for symbolic links
--------------------------

Used for example to create a directory with links from *gcc* to *gcc-x.y* to override compilers
"""
import os
import os.path
import subprocess
import re
from ApeTools import Config, Build
from ApeTools.Build import InstallError
from ConfigParser import NoOptionError

class SymLink(Build.Package):
    def __init__(self, name, builder):
        super(SymLink, self).__init__(name, builder)
        self.infoVariables = "name prefix version _env isNeeded linkNames targetNames linkSource".split()
        try:
            self.testCommand = Config.get(self.section, 'testCommand').split()
        except NoOptionError:
            self.testCommand = []
        if self.testCommand:
            try:
                self.testRe = re.compile(Config.get(self.section, 'testRe'))
            except NoOptionError:
                raise InstallError(args=['Package configuration error'], pack=self.name, stage='Package constructor')

            testOutput = subprocess.Popen(self.testCommand, stdout=subprocess.PIPE, stderr=subprocess.PIPE).communicate()[0]
            self.isNeeded = bool(self.testRe.search(testOutput))
        else:
            self.isNeeded = True

        self.linkNames = Config.getlist(self.section, 'linkNames')
        self.targetNames = dict([(link, Config.get(self.section, 'target.'+link)) for link in self.linkNames])
        self.linkSource = Config.getlist(self.section, 'linkSource')

    def isDummy(self):
        return False

    def isInstalled(self):
        return not self.isNeeded or os.path.exists(self.prefix)

    def unpack(self, parentDir=None):
        pass

    def build(self, env):
        if self.isNeeded:
            print "  ... Setting links"
            linkBin = os.path.join(self.prefix, 'bin')
            if not os.path.exists(linkBin):
                os.makedirs(linkBin)
            for link, target in self.targetNames.iteritems():
                for src in self.linkSource:
                    t = os.path.join(src, target)
                    if os.path.exists(t):
                        os.symlink(t, os.path.join(linkBin, link))
                        break
