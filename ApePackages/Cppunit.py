"""
Build support for cppunit_ testing package
------------------------------------------
"""
import sys

from ApeTools import Build
from ApeTools import InstallError


class Cppunit(Build.Package):

    # HACK: configure fails on os x ==> ignore errors during configuration
    def configure(self, logFile, env):
        """Configuration fails on OS X, at least for version ``1.12.1``.
        Nevertheless, it is possible to build the package with the files
        which got created. So we catch and ignore all errors in the *configure*
        phase if we are on OS X.
        """
        try:
            Build.Package.configure(self, logFile, env)
        except InstallError:
            if sys.platform != "darwin":
                raise
