"""
Build support for the xercesc_ xml parser
-----------------------------------------

The builder is registered with the name ``xerces``.
"""
import sys

from ApeTools import Build
from ApeTools.Build import InstallError


class Xerces(Build.Package):
    """Add the attribute :attr:`sourceDirectory` and read it from the
    configuration.
    """
    def __init__(self, name):
        Build.Package.__init__(self, name)
        self.setAttributes("sourceDirectory")

    def build(self, env):
        """Set the environment variable :envvar:`XERCESCCROOT` to the
        :attr:`sourceDirectory` before calling
        :meth:`ApeTools.Build.Package.build` to build xercesc_.
        """
        env.update(XERCESCROOT=self.sourceDirectory)
        if not self.configureArgs:
            raise InstallError(pack="Xerces",
                               args=["Unknown platform", sys.platform],
                               stage="prepare build")
        Build.Package.build(self, env)
