Specialized build support for individual packages: :mod:`ApePackages`
=======================================================================

This module contains sub-modules for the packages which require special
code for building. The default implementation for all the build steps is
in :class:`ApeTools.Build.Package`. Packages which are not listed here are
configured using only the options documented in the section
:ref:`configuration-files`.

The name of the builder is normaly the name of the package, in lower-case
letters. Exceptions are noted in the corresponding builder modules.

All sub-modules of :mod:`ApePackages` are imported to give them a chance to
register their creator functions.

.. automodule:: ApePackages
   :members:
   :undoc-members:


.. automodule:: ApePackages.ADST
   :members:
   :undoc-members:

.. _adst: http://augerobserver.fzk.de/doku.php?id=adstsoftware


.. automodule:: ApePackages.Aires

   .. autoclass:: Aires
      :members: __init__, configure, make
      :undoc-members:

.. _aires: http://www.fisica.unlp.edu.ar/auger/aires/


.. automodule:: ApePackages.AugerCMake
   :members:
   :undoc-members:

.. _cmake: http://www.cmake.org/


.. automodule:: ApePackages.CDAS

   .. autoclass:: CDAS
      :members: __init__, packageVersion, env, unpack, isInstalled
      :undoc-members:

.. _cdas: http://www.auger.org.ar/CDAS/

.. _CMT: http://www.cmtsite.org/


.. automodule:: ApePackages.Cppunit
   :members:
   :undoc-members:

.. _cppunit: http://sourceforge.net/projects/cppunit


.. automodule:: ApePackages.Mysql
   :members:
   :undoc-members:

.. _MySQL: http://www.mysql.com/

.. _MySQL download site: http://dev.mysql.com/downloads/connector/c/6.0.html


.. automodule:: ApePackages.Root

   .. autoclass:: Root
      :members: __init__, build, unpack
      :undoc-members:

.. _root: http://root.cern.ch/


.. automodule:: ApePackages.Xerces
   :members:
   :undoc-members:

.. _xercesc: http://xerces.apache.org/xerces-c/
