#! /usr/bin/env python

# Configure path to find modules to test
import sys
import os.path
if __name__ == "__main__":
    sys.path.insert(0, os.path.dirname(sys.path[0]))

import unittest
from ApeTools.Environment import Environment


class TestEnvironment(unittest.TestCase):
    ref = {"RC": "ape.rc"}
    base = {"ROOT": "/base"}

    def setUp(self):
        self.env = Environment(self.ref, self.base)

    def testCreate(self):
        env = Environment()
        self.assertEqual(env._env, {})
        self.assertEqual(env._baseEnv, {})
        self.assertEqual(set(env.keys()), set())

    def testCreateEnv(self):
        env = Environment(self.ref)
        self.assertEqual(env._env, self.ref)
        self.assertEqual(env._baseEnv, {})
        self.assertEqual(set(env.keys()), set(["RC"]))

    def testCreateBase(self):
        env = Environment(baseEnv=self.base)
        self.assertEqual(env._env, {})
        self.assertEqual(env._baseEnv, self.base)
        self.assertEqual(set(env.keys()), set(["ROOT"]))

    def testCreateEnvBase(self):
        env = Environment(self.ref, self.base)
        self.assertEqual(env._env, self.ref)
        self.assertEqual(env._baseEnv, self.base)
        self.assertEqual(set(env.keys()), set(["RC", "ROOT"]))

    def testGet(self):
        self.assertEqual(self.env["RC"], "ape.rc")
        self.assertEqual(self.env.get("RC"), "ape.rc")
        self.assertEqual(self.env["ROOT"], "/base")
        self.assertEqual(self.env.get("ROOT"), "/base")
        self.assertRaises(KeyError, lambda x: self.env[x], "None")
        self.assertEqual(self.env.get("None"), None)
        self.assertEqual(self.env.get("Some", "some"), "some")

    def testContains(self):
        self.assertTrue("RC" in self.env)
        self.assertTrue("ROOT" in self.env)
        self.assertTrue("None" not in self.env)

    def testDelEnv(self):
        del self.env["RC"]
        self.assertFalse("RC" in self.env)
        self.assertEqual(self.env._env, {})
        self.assertRaises(KeyError, lambda x: self.env[x], "RC")
        self.assertRaises(KeyError, lambda x: self.env.__delitem__(x), "RC")

    def testDelBase(self):
        del self.env["ROOT"]
        self.assertFalse("ROOT" in self.env)
        self.assertEqual(self.env._baseEnv, self.base)
        self.assertEquals(self.env._deleted, set(["ROOT"]))
        self.assertRaises(KeyError, lambda x: self.env[x], "ROOT")
        self.assertRaises(KeyError, lambda x: self.env.__delitem__(x), "ROOT")

    def testSetDelEnv(self):
        self.env["NEW"] = "item"
        self.assertEqual(self.env["NEW"], "item")
        del self.env["NEW"]
        self.assertTrue("NEW" not in self.env)
        self.assertEqual(self.env._env, self.ref)
        self.assertEqual(self.env._baseEnv, self.base)
        self.env["RC"] = "my.rc"
        self.assertEqual(self.env["RC"], "my.rc")
        self.assertEqual(self.env._env, {"RC": "my.rc"})
        self.assertEqual(self.env._baseEnv, self.base)

    def testSetBase(self):
        self.env["ROOT"] = "/new.root"
        self.assertEquals(self.env["ROOT"], "/new.root")
        self.assertEqual(self.env._env, dict(self.ref, ROOT="/new.root"))
        self.assertEqual(self.env._baseEnv, self.base)

    def testIteratorKeys(self):
        self.assertEqual(len(list(self.env)), 2)
        self.assertEqual(set(self.env), set(["RC", "ROOT"]))
        self.assertEqual(len(list(self.env.keys())), 2)
        self.assertEqual(set(self.env.keys()), set(["RC", "ROOT"]))

        self.env["NEW"] = "item"
        self.assertEqual(len(list(self.env)), 3)
        self.assertEqual(set(self.env), set(["RC", "ROOT", "NEW"]))
        self.assertEqual(len(list(self.env.keys())), 3)
        self.assertEqual(set(self.env.keys()), set(["RC", "ROOT", "NEW"]))

        self.env["RC"] = "my.rc"
        self.assertEqual(len(list(self.env)), 3)
        self.assertEqual(set(self.env), set(["RC", "ROOT", "NEW"]))
        self.assertEqual(len(list(self.env.keys())), 3)
        self.assertEqual(set(self.env.keys()), set(["RC", "ROOT", "NEW"]))

        self.env["ROOT"] = "/new.root"
        self.assertEqual(len(list(self.env)), 3)
        self.assertEqual(set(self.env), set(["RC", "ROOT", "NEW"]))
        self.assertEqual(len(list(self.env.iteritems())), 3)
        self.assertEqual(set(self.env.iteritems()), set([("RC", "my.rc"),
                                                         ("ROOT", "/new.root"),
                                                         ("NEW", "item")]))
        self.assertEqual(len(list(self.env.keys())), 3)
        self.assertEqual(set(self.env.keys()), set(["RC", "ROOT", "NEW"]))

        del self.env["ROOT"]
        self.assertEqual(len(list(self.env)), 2)
        self.assertEqual(set(self.env), set(["RC", "NEW"]))
        self.assertEqual(len(list(self.env.iteritems())), 2)
        self.assertEqual(set(self.env.iteritems()), set([("RC", "my.rc"),
                                                         ("NEW", "item")]))
        self.assertEqual(len(list(self.env.keys())), 2)
        self.assertEqual(set(self.env.keys()), set(["RC", "NEW"]))

        del self.env["NEW"]
        self.assertEqual(len(list(self.env)), 1)
        self.assertEqual(set(self.env), set(["RC"]))
        self.assertEqual(len(list(self.env.iteritems())), 1)
        self.assertEqual(set(self.env.iteritems()), set([("RC", "my.rc")]))
        self.assertEqual(len(list(self.env.keys())), 1)
        self.assertEqual(set(self.env.keys()), set(["RC"]))

        del self.env["RC"]
        self.assertEqual(len(list(self.env)), 0)
        self.assertEqual(set(self.env), set())
        self.assertEqual(len(list(self.env.iteritems())), 0)
        self.assertEqual(set(self.env.iteritems()), set())
        self.assertEqual(len(list(self.env.keys())), 0)
        self.assertEqual(set(self.env.keys()), set())

    def testExtendScalar(self):
        ref1 = {"RC": "my.rc"}
        ref2 = {"ROOT": "/new.root"}
        self.env.update(ref1)
        self.env.update(ref2)
        self.assertEqual(dict(self.env), dict(ref1.items() + ref2.items()))
        self.env.update(IN="file.rc")
        self.assertEqual(dict(self.env), dict(ref1.items() + ref2.items() +
                                              [("IN", "file.rc")]))

    def testExtendPathEmpty(self):
        env = Environment({}, {})
        p1 = {"PATH": "/a"}
        env.update(p1)
        self.assertEqual(env._env, p1)
        self.assertEqual(env["PATH"], "/a")
        self.assertEqual(env.sh(), "export PATH=/a")
        self.assertEqual(env.csh(), "setenv PATH /a;")
        p2 = {"PATH": "/b"}
        env.update(p2)
        self.assertEqual(env._env, {"PATH": "/b:/a"})
        self.assertEqual(env["PATH"], "/b:/a")
        self.assertEqual(env.sh(), "export PATH=/b:/a")
        self.assertEqual(env.csh(), "setenv PATH /b:/a;")

    def testExtendPath(self):
        env = Environment({}, {"PATH": "/bin"})
        p1 = {"PATH": "/a"}
        env.update(p1)
        self.assertEqual(env._env, {"PATH": "/a:/bin"})
        self.assertEqual(env["PATH"], "/a:/bin")
        self.assertEqual(env.sh(), "export PATH=/a:/bin")
        self.assertEqual(env.csh(), "setenv PATH /a:/bin;")

    def testExtendSysPathEmpty(self):
        env = Environment({}, {})
        p1 = {"MANPATH": "/a"}
        env.update(p1)
        self.assertEqual(env._env, {"MANPATH": "/a:"})
        self.assertEqual(env["MANPATH"], "/a:")
        self.assertEqual(env.sh(), "export MANPATH=/a:")
        self.assertEqual(env.csh(), "setenv MANPATH /a:;")
        p2 = {"MANPATH": "/b"}
        env.update(p2)
        self.assertEqual(env._env, {"MANPATH": "/b:/a:"})
        self.assertEqual(env["MANPATH"], "/b:/a:")
        self.assertEqual(env.sh(), "export MANPATH=/b:/a:")
        self.assertEqual(env.csh(), "setenv MANPATH /b:/a:;")

    def testExtendSysPath(self):
        env = Environment({}, {"MANPATH": "/man"})
        p1 = {"MANPATH": "/a"}
        env.update(p1)
        self.assertEqual(env._env, {"MANPATH": "/a:/man"})
        self.assertEqual(env["MANPATH"], "/a:/man")
        self.assertEqual(env.sh(), "export MANPATH=/a:/man")
        self.assertEqual(env.csh(), "setenv MANPATH /a:/man;")

    def testExtendFromEnv(self):
        env = Environment()
        env.update(self.env)
        self.assertEqual(len(env), 2)
        self.assertEqual(dict(env), dict(self.env))
        self.assertEqual(env._env, dict(self.env))

if __name__ == '__main__':
    unittest.main(testRunner=unittest.TextTestRunner(verbosity=2))
